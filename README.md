# Silly Giphy Utility in Rust

## Usage

```console
$ giphy -s https://giphy.com/gifs/excited-8p8E1sylIARDW
id: 8p8E1sylIARDW
url: https://media.giphy.com/media/8p8E1sylIARDW/giphy.gif
output: giphy-8p8E1sylIARDW.gif
```

## Help

```console
$ giphy --help
giphy 0.1.0
Travis Miller <travis@travismiller.com>
Silly utility for Giphy

USAGE:
    giphy [FLAGS] <id>

FLAGS:
    -h, --help       Prints help information
    -s, --save       Saves to the current directory
    -V, --version    Prints version information

ARGS:
    <id>    Specify the ID
```