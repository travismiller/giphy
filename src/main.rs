#[macro_use] extern crate clap;
extern crate regex;
extern crate reqwest;

use clap::Arg;
use regex::Regex;

fn extract_id_from_url(url: &str) -> Option<String> {
    let re = Regex::new(r"(?x)
        (?:^|(?:\S*(?:gifs|media|gph\.is))?/)
        (?:[^/]+-)?
        (?P<id>[0-9a-zA-Z]+?)
        (?:$|/\S+)
    ").expect("valid regex");

    re.captures(url.trim()).map(|captures| {
        captures
            .name("id")
            .expect("<id> is a named group")
            .as_str()
            .to_string()
    })
}

fn build_download_url(id: &str) -> String {
    format!("https://media.giphy.com/media/{}/giphy.gif", id)
}

fn main() {
    let arg_save = Arg::with_name("save")
        .help("Saves to the current directory")
        .short("s")
        .long("save");

    let arg_id = Arg::with_name("id")
        .help("Specify the ID")
        .required(true);

    let arg_outfile = Arg::with_name("outfile")
        .help("Output file path. Use \"-\" for stdout.");

    let matches = app_from_crate!()
        .arg(arg_save)
        .arg(arg_id)
        .arg(arg_outfile)
        .get_matches();

    let id = matches.value_of("id").expect("<id> as argument");
    let id = extract_id_from_url(id).expect("<id> as a valid Giphy ID");
    let url = build_download_url(&id);
    let default_path = format!("giphy-{}.gif", &id);

    eprintln!("id: {}", &id);
    eprintln!("url: {}", &url);

    let mut resp = reqwest::get(&url).unwrap();

    if matches.is_present("save") {
        eprintln!("output: {}", &default_path);
        let out = &mut std::fs::File::create(default_path).unwrap();
        resp.copy_to(out).unwrap();
    };

    if let Some(outfile) = matches.value_of("outfile") {
        match outfile.trim() {
            "" => {},
            "-" => {
                let out = &mut std::io::stdout();
                resp.copy_to(out).unwrap();
            },
            _ => {
                eprintln!("output: {}", &outfile);
                let out = &mut std::fs::File::create(outfile).unwrap();
                resp.copy_to(out).unwrap();
            },
        }
    };
}

#[cfg(test)]
mod tests {
    use super::*;

    // TODO: Support links:short: http://gph.is/VwN1Vl

    #[test]
    fn test_extract_id_from_url() {
        let id = Some("8p8E1sylIARDW".to_string());

        // id: 8p8E1sylIARDW
        assert_eq!(id, extract_id_from_url("8p8E1sylIARDW"));

        // categorized id : excited-8p8E1sylIARDW
        assert_eq!(id, extract_id_from_url("excited-8p8E1sylIARDW"));

        // canonical: https://giphy.com/gifs/excited-8p8E1sylIARDW
        assert_eq!(id, extract_id_from_url("https://giphy.com/gifs/excited-8p8E1sylIARDW"));

        // alternative: https://giphy.com/gifs/8p8E1sylIARDW
        assert_eq!(id, extract_id_from_url("https://giphy.com/gifs/8p8E1sylIARDW"));

        // links: https://giphy.com/gifs/excited-8p8E1sylIARDW/links
        assert_eq!(id, extract_id_from_url("https://giphy.com/gifs/excited-8p8E1sylIARDW/links"));

        // links:html5: https://giphy.com/gifs/8p8E1sylIARDW/html5
        assert_eq!(id, extract_id_from_url("https://giphy.com/gifs/8p8E1sylIARDW/html5"));

        // media: https://giphy.com/gifs/excited-8p8E1sylIARDW/media
        assert_eq!(id, extract_id_from_url("https://giphy.com/gifs/excited-8p8E1sylIARDW/media"));

        // media:source: https://media.giphy.com/media/8p8E1sylIARDW/giphy.gif
        // media:social: https://media.giphy.com/media/8p8E1sylIARDW/giphy.gif
        assert_eq!(id, extract_id_from_url("https://media.giphy.com/media/8p8E1sylIARDW/giphy.gif"));

        // media:mp4: https://media.giphy.com/media/8p8E1sylIARDW/giphy.mp4
        assert_eq!(id, extract_id_from_url("https://media.giphy.com/media/8p8E1sylIARDW/giphy.mp4"));

        // media:small: https://media.giphy.com/media/8p8E1sylIARDW/200w_d.gif
        assert_eq!(id, extract_id_from_url("https://media.giphy.com/media/8p8E1sylIARDW/200w_d.gif"));
    }
}